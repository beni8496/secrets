# secrets analyzer

secrets analyzer performs Secret Detection scanning. It reports possible secret leaks, like application tokens and cryptographic keys, in the source code and files contained in your project.

The analyzer wraps [Gitleaks](https://github.com/zricethezav/gitleaks) tool, and is written in Go. It's structured similarly to other Static Analysis analyzers because it uses the shared [command](https://gitlab.com/gitlab-org/security-products/analyzers/command) package.

The analyzer is built and published as a Docker image in the GitLab Container Registry associated with this repository. You would typically use this analyzer in the context of a [SAST](https://docs.gitlab.com/ee/user/application_security/sast), [IaC](https://docs.gitlab.com/ee/user/application_security/iac_scanning), or [Secret Detection](https://docs.gitlab.com/ee/user/application_security/secret_detection) job in your CI/CD pipeline. However, if you're contributing to the analyzer or you need to debug a problem, you can run, debug, and test locally using Docker.

For instructions on local development, please refer to the [README in Analyzer Scripts](https://gitlab.com/gitlab-org/secure/tools/analyzer-scripts/-/blob/master/analyzers-common-readme.md).

## Versioning and release process

Please check the [versioning and release process documentation](https://gitlab.com/gitlab-org/security-products/analyzers/common#versioning-and-release-process).

## Contributing

Contributions are welcome, see [`CONTRIBUTING.md`](CONTRIBUTING.md) for more details.

## Adding rules

New rules can be added to the `./gitleaks.toml` file. So that the new rule doesn't generate excessive false positives, we recommend adding word boundries `\b` to the beginning of the regular expression and either `\b` or `['|\"|\n|\r|\s|\x60]` to the end. Choosing between ending the regular expression with `\b` or `['|\"|\n|\r|\s|\x60]` will depend on the complexity of the regular expression and the likelihood that using `\b` will result in excessive false positives. If you're unsure, test both and review the difference in the [BAP CI job](https://gitlab.com/gitlab-org/security-products/analyzers/secrets/-/blob/f33f86c7e4a2311086ea927558a285ee7951ccfd/.gitlab-ci.yml#L109).

To ensure the rule is working as expected, add an example token to `qa/fixtures/secrets/secrets.go` and then refresh the expected JSON using `analyzer-refresh-expected-json` from [analyzer scripts](https://gitlab.com/gitlab-org/secure/tools/analyzer-scripts/-/blob/master/analyzers-common-readme.md). Ensure the test token is included in the report.

### Tags

The `tags` field for a given rule is a gitleaks-native array of strings used for metadata and reporting purposes. We use several predefined tags, some for organizational purposes and others for special processing:

- `gitlab` - An organizational tag for grouping GitLab-specific rules for organizational purposes
- `gitlab_partner_token` - A organizational tag for grouping GitLab-partner verified rules
- `revocation_type` - A processing tag used for identifing rules linked to our [automatic response to leaked secrets](https://docs.gitlab.com/ee/user/application_security/secret_detection/automatic_response.html) capability
- `gitlab_blocking` - A processing tag used to identify secrets which are processed as part of our [platform-wide experience](https://docs.gitlab.com/ee/architecture/blueprints/secret_detection/) in a blocking manner (i.e. pre-receive).

The `revocation_type` is a special tag that relies on the rules to have static IDs. These rule IDs must precisely map to the [token types](https://gitlab.com/gitlab-com/gl-security/engineering-and-research/automation-team/secret-revocation-service/-/blob/d14092c43574c0ed518a1e30cac6bcc17721eb99/srs/__init__.py#L31) as defined in our Secret Revocation Service to ensure revocation works as expected.

## License

This code is distributed under the MIT Expat license, see the [LICENSE](LICENSE) file.
